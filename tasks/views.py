# from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Task
from datetime import date


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "project",
        "assignee",
    ]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        d = self.request.GET.get("dir", "")
        context["dir"] = "-" if d == "" else ""
        context["arrow"] = "▲" if d == "" else "▼"
        return context

    def get_queryset(self):
        q = self.request.GET.get("q", "is_completed")
        d = self.request.GET.get("dir", "")

        return Task.objects.filter(assignee=self.request.user).order_by(d + q)


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")
